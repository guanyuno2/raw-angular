# Stage 1: Build Angular application
FROM node:18-alpine AS build

# Set working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install Angular CLI globally
RUN npm install -g @angular/cli

# Install dependencies
RUN npm install

# Copy the rest of the application
COPY . .

# Expose port 80 for web server access (nginx default)
EXPOSE 4200

# Start nginx web server (default command for nginx image)
CMD ["ng", "serve", "--host", "0.0.0.0"]