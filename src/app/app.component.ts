import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AuthenticationService } from './oidc/services';
import { IUserInfo } from './oidc/services/iuser-info';
import { environment } from '../environments/environment.development';
import { NavBarComponent } from './nav-bar/nav-bar.component';

@Component({
	selector: 'app-root',
	standalone: true,
	imports: [RouterOutlet, NavBarComponent],
	templateUrl: './app.component.html',
	styleUrl: './app.component.css'
})
export class AppComponent {
	title = 'raw-angular';
	userInfo?: IUserInfo

	constructor(
		private readonly _authService: AuthenticationService
	) {
		this.userInfo = _authService.getUserInfo()
		console.log("env", environment.production);
		
	}

}
