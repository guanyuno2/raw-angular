import { Injectable } from '@angular/core';
import { User, UserManager, WebStorageStateStore } from 'oidc-client';
import { UserManagerSettings } from '../models';
import { IUserInfo } from './iuser-info';
import { environment } from '../../../environments/environment';

type Nullable<T> = T | null

@Injectable({
	providedIn: 'root'
})
export class AuthenticationService {
	isUserDefined = false;
	private _user: Nullable<User>;
	private _userManager: UserManager;

	getUserInfo() {
		let val = {
			name: this._user?.profile?.name ?? ""
		} as IUserInfo
		return val;
	}

	isLoggedIn() {
		return this._user != null && !this._user.expired;
	}

	getAccessToken() {
		return this._user ? this._user.access_token : '';
	}

	getClaims() {
		return this._user?.profile;
	}

	startAuthentication(): Promise<void> {
		this.getUserManager();
		return this._userManager.signinRedirect();
	}

	async completeAuthentication() {
		this.getUserManager();
		var user = await this._userManager.signinRedirectCallback();
		if (user) {
			this._user = user;
			this.isUserDefined = true;
		}
	}


	startLogout(): Promise<void> {
		this.getUserManager();
		return this._userManager.signoutRedirect();
	}

	completeLogout() {
		this.getUserManager();
		this._user = null;
		return this._userManager.signoutRedirectCallback();
	}


	silentSignInAuthentication() {
		this.getUserManager();
		return this._userManager.signinSilentCallback();
	}


	private getUserManager() {
		if (!this._userManager) {
			const userManagerSettings: UserManagerSettings =
				new UserManagerSettings();

			console.log("environment", environment);
			

			//set up settings
			userManagerSettings.authority = 'https://account.iofty.online'; //website that responsible for Authentication
			userManagerSettings.client_id = 'web-angular'; //uniqe name to identify the project
			userManagerSettings.response_type = 'code'; //desired Authentication processing flow - for angular is sutible code flow
			//specify the access privileges, specifies the information returned about the authenticated user.
			userManagerSettings.scope = 'openid profile api1';

			userManagerSettings.redirect_uri = environment.redirect_uri; //start login process
			userManagerSettings.post_logout_redirect_uri = 'https://social.iofty.online/logout-callback'; //start logout process

			userManagerSettings.automaticSilentRenew = true;
			userManagerSettings.silent_redirect_uri = 'https://social.iofty.online/silent-callback'; //silent renew oidc doing it automaticly 

			userManagerSettings.userStore = new WebStorageStateStore({
				store: window.localStorage,
			}); // store information about Authentication in localStorage

			this._userManager = new UserManager(userManagerSettings);

			this._userManager.getUser().then((user) => {

				this._user = user;
				this.isUserDefined = true;
			});
		}
	}

}
