
import { Injectable, inject } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivateFn, Router, RouterStateSnapshot
} from '@angular/router';
import {
    AuthenticationService
} from '../services';

// @Injectable()
// export class AuthGuard implements CanActivate {
//     constructor(
//         private readonly _authService: AuthenticationService,
//     ) {}

//     async canActivate() {
//         if (this._authService.isLoggedIn()) {
//             return true;
//         }

//         await this._authService.startAuthentication();
//         return false;
//     }
// }


@Injectable({
    providedIn: 'root'
})
class PermissionsService {

    constructor(
        private readonly _authService: AuthenticationService
    ) { }

    async canActivate(): Promise<boolean> {
        if (this._authService.isLoggedIn()) {
            return true;
        }

        await this._authService.startAuthentication();
        return false;
    }
}

export const AuthGuard: CanActivateFn = () => {
    return inject(PermissionsService).canActivate();
}