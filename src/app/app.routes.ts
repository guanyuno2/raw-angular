import { Routes } from '@angular/router';
import { FirstComponent } from './first/first.component';
import { AuthGuard } from './oidc/guards';
import { LoginCallbackComponent } from './oidc/components';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';

export const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: AppComponent
    },
    {
        path: 'home-page',
        canActivate: [AuthGuard],
        component: HomePageComponent
    },
    {
        path: 'login',
        canActivate: [AuthGuard],
        component: HomePageComponent
    },
    {
        path: 'first',
        component: FirstComponent
    },
    {
        path: 'login-callback',
        component: LoginCallbackComponent,
    }
];
