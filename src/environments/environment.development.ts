export const environment = {
    production: false,
    authority: "https://account.iofty.online",
    redirect_uri: "http://localhost:4200/login-callback",
    post_logout_redirect_uri: "http://localhost:4200/logout-callback",
    silent_redirect_uri: "http://localhost:4200/silent-callback"
};
