export const environment = {
    production: true,
    authority: "https://account.iofty.online",
    redirect_uri: "https://social.iofty.online/login-callback",
    post_logout_redirect_uri: "https://social.iofty.online/logout-callback",
    silent_redirect_uri: "https://social.iofty.online/silent-callback"
};
